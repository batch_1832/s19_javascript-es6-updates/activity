// console.log("Hello");

// 3.

let getCube = 2 ** 3;
console.log(getCube);

// 4.
console.log(`The cube of 2 is ${getCube}`);

// 5.
let address = ["258 Washington Ave", "NW", "California", "90011"];

// 6.
console.log (`I live at ${address[0]} ${address[1]}, ${address[2]} ${address[3]}`);

//7. 
let animal = {
	name: "Lolong",
	type: "Sea Water Crocodile",
	weight: "1075 kgs",
	length: "20 ft 3 in."
}

// 8. 

console.log(`${animal.name} was a ${animal.type}. He weighed at ${animal.weight} with a measurement of ${animal.length}`);

// 9. 
numbersArr = [1, 2, 3, 4, 5]

// 10.

// numbersArr.forEach(function(number){
// 	return console.log(number);
// })

numbersArr.forEach((number) => {
	console.log(`${number}`)
})

// 11. 

let reduceNumber = (a,b,c,d,e) => a+b+c+d+e;

let total = reduceNumber(1,2,3,4,5)

console.log(`${total}`);


// 12.

class dog {
	constructor(name, age, breed){
		this.name = name
		this.age = age
		this.breed = breed
	}
	
}

// 13. 

let myDog = new dog


myDog.name = "Frankie"
myDog.age = 5
myDog.breed = "Miniature Dachshund"


console.log(myDog);